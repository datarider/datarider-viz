# INIT
source .env

# DOCKER
source .env
## START containers
docker compose up -d

## WAIT to have Superset ready
echo "[INFO] Wait Superset to be ready"
sleep 15


# SUPERSET
## INSTALL jq (if not available)
LIB_INSTALL=$(dpkg --list | grep -c jq)
if [ $LIB_INSTALL -lt 1 ]
then
  echo "[INFO] Install jq (first time only)"
  sudo apt-get install jq
fi
## INSTALL httpie (if not available)
LIB_INSTALL=$(dpkg --list | grep -c httpie)
if [ $LIB_INSTALL -lt 1 ]
then
  echo "[INFO] Install httpie (first time only)"
  sudo apt install httpie
fi

## AUTHENTICATE and acquire a JWT token.
AUTH_TOKEN=$(http "http://$LOCALHOST_URL:$SUPERSET_PORT/api/v1/security/login" username=$SUPERSET_ADMIN_USERNAME password=$SUPERSET_ADMIN_PASSWORD provider=db | jq -r .access_token)

## CREATE a data source item / database connection.
echo "[INFO] Add Clickhouse connection to Superset"
http "http://$LOCALHOST_URL:$SUPERSET_PORT/api/v1/database/" database_name="Datarider Clickhouse" engine=clickhouse sqlalchemy_uri=clickhousedb://${CLICKHOUSE_HOST_NAME}:${CLICKHOUSE_PORT_HTTP}/dr_gold Authorization:"Bearer ${AUTH_TOKEN}"


# OPEN web pages
## OPEN Clickhouse UI
echo "[INFO] Clickhouse UI is opening in your default browser"
xdg-open "http://$LOCALHOST_URL:$CLICKHOUSEUI_PORT"

## OPEN Superset
echo "[INFO] Superset is opening in your default browser"
xdg-open "http://$LOCALHOST_URL:$SUPERSET_PORT"

