FEATURE_FLAGS = {
    "ENABLE_TEMPLATE_PROCESSING": True
}

ENABLE_PROXY_FIX = True
SECRET_KEY = "4U50MwoSHcc1ypuw6FB8I18C/vUCtCOcyozq34meHY2CqQJIsfDotVNW"

# To be tested clickhouse+native://datarider_clickhouse:8123/dr_gold
#SQLALCHEMY_DATABASE_URI = "clickhousedb://datarider_clickhouse:8123/dr_gold"  # cause Superset error (so we use API to init connection)
#SQLALCHEMY_ECHO = True
# https://github.com/ClickHouse/clickhouse-connect

WTF_CSRF_ENABLED = False # Do not disabled in PRODUCTION
