-- CLEAN existing database
DROP DATABASE IF EXISTS dr_gold;

-- CREATE database for Data Rider Gold data
CREATE DATABASE IF NOT EXISTS dr_gold;
