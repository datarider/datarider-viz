SET format_csv_delimiter = '|';
INSERT INTO dr_gold.dim_player SELECT * FROM file('/var/lib/clickhouse/user_files/dim_player.csv', 'CSVWithNames');
INSERT INTO dr_gold.dim_event SELECT * FROM file('/var/lib/clickhouse/user_files/dim_event.csv', 'CSVWithNames');
INSERT INTO dr_gold.dim_session SELECT * FROM file('/var/lib/clickhouse/user_files/dim_session.csv', 'CSVWithNames');
INSERT INTO dr_gold.dim_track SELECT * FROM file('/var/lib/clickhouse/user_files/dim_track.csv', 'CSVWithNames');
INSERT INTO dr_gold.player_session SELECT * FROM file('/var/lib/clickhouse/user_files/player_session.csv', 'CSVWithNames');
INSERT INTO dr_gold.etl_agg_track SELECT * FROM file('/var/lib/clickhouse/user_files/etl_agg_track.csv', 'CSVWithNames');

INSERT INTO dr_gold.etl_event_session_player_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_event_session_player_v.csv', 'CSVWithNames');
INSERT INTO dr_gold.etl_turns_sections_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_turns_sections_v.csv', 'CSVWithNames');
INSERT INTO dr_gold.etl_turns_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_turns_v.csv', 'CSVWithNames');

INSERT INTO dr_gold.etl_all_session_player_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_all_session_player_v.csv', 'CSVWithNames');
INSERT INTO dr_gold.etl_all_session_turns_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_all_session_turns_v.csv', 'CSVWithNames');
INSERT INTO dr_gold.etl_event_last_session_player_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_event_last_session_player_v.csv', 'CSVWithNames');
INSERT INTO dr_gold.etl_event_session_player_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_event_session_player_v.csv', 'CSVWithNames');
INSERT INTO dr_gold.etl_last_session_turns_v SELECT * FROM file('/var/lib/clickhouse/user_files/etl_last_session_turns_v.csv', 'CSVWithNames');
