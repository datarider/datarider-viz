-- dim_player
CREATE TABLE IF NOT EXISTS dr_gold.dim_player (
  id Int16,
  name String,
  email String,
  created_at DateTime('Europe/Paris'),
  updated_at DateTime('Europe/Paris')
) ENGINE = MergeTree
ORDER BY
    (id);


-- dim_session
CREATE TABLE IF NOT EXISTS dr_gold.dim_session (
  id Int16,
  event_id Int16,
  begin_at DateTime('Europe/Paris'),
  end_at Nullable(
    DateTime('Europe/Paris')
  )
) ENGINE = MergeTree
ORDER BY
    (event_id, begin_at);


-- dim_event
CREATE TABLE IF NOT EXISTS dr_gold.dim_event (
  id Int16,
  name String,
  begin_at DateTime('Europe/Paris'),
  end_at DateTime('Europe/Paris')
) ENGINE = MergeTree
ORDER BY
    (id);

-- dim_track
CREATE TABLE IF NOT EXISTS dr_gold.dim_track (
  track_num Int8,
  gate Int8,
  length_m Float32,
  section_num Int8
) ENGINE = MergeTree
ORDER BY
    (track_num, gate);

-- player_session
CREATE TABLE IF NOT EXISTS dr_gold.player_session (
  session_id Int16,
  player_id Int16,
  track_num Int8
) ENGINE = MergeTree
ORDER BY
    (session_id, player_id);


CREATE TABLE IF NOT EXISTS dr_gold.etl_agg_track
(
  track_num UInt8,
  turn UInt8,
  gate_active_id UInt8,
  event_ts_second DateTime('Europe/Paris'),
  start_ts DateTime64(3, 'Europe/Paris'), -- milli seconde
  duration_ms UInt16,
  co2_ug Float32,
  is_outoftrack Boolean,
  __inserted_time DateTime DEFAULT now()
) ENGINE = MergeTree()
PRIMARY KEY (track_num, turn, gate_active_id, event_ts_second);



CREATE TABLE IF NOT EXISTS dr_gold.etl_event_session_player_v
(
  `track_num` UInt8,
  `turn` UInt8,
  `gate_active_id` UInt8,
  `event_ts_second` DateTime('Europe/Paris'),
  `start_ts` DateTime64(3, 'Europe/Paris'),
  `duration_ms` UInt16,
  `co2_ug` Float32,
  `is_outoftrack` Boolean,
  `__inserted_time` DateTime DEFAULT now(),
  `session_id` UInt16,
  `event_id` UInt16,
  `player_id` UInt16,
  `name` String
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn, gate_active_id, event_ts_second);

CREATE TABLE IF NOT EXISTS dr_gold.etl_turns_sections_v
(
   `session_id` UInt16,
   `event_id` UInt16,
   `player_id` UInt16,
   `name` String,
  `track_num` UInt8,
  `turn` UInt8,
  `section` UInt8,
  `duration_s` Float32,
  `co2_ug` Float32,
  `nb_outoftrack` UInt16
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn, section);

CREATE TABLE IF NOT EXISTS dr_gold.etl_turns_v
(
   `session_id` UInt16,
   `event_id` UInt16,
   `player_id` UInt16,
   `name` String,
  `track_num` UInt8,
  `turn` UInt8,
  `duration_ms` UInt16,
  `co2_ug` Float32,
  `nb_outoftrack` UInt16
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn);



CREATE TABLE IF NOT EXISTS dr_gold.etl_all_session_player_v
(
    `track_num` UInt8,
    `turn` UInt8,
    gate_active_id UInt8,
    event_ts_second DateTime('Europe/Paris'),
    start_ts DateTime64(3, 'Europe/Paris'), -- milli seconde
  `duration_ms` UInt16,
    `co2_ug` Float32,
      is_outoftrack Boolean,
      __inserted_time DateTime('Europe/Paris'),
   `session_id` UInt16,
   `event_id` UInt16,
   `player_id` UInt16,
   `name` String
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn);


CREATE TABLE IF NOT EXISTS dr_gold.etl_all_session_turns_v
(
       `session_id` UInt16,
       `event_id` UInt16,
       `player_id` UInt16,
       `name` String,
    `track_num` UInt8,
    `turn` UInt8,
    `duration_ms` UInt16,
    gate_active_id UInt8,
    event_ts_second DateTime('Europe/Paris'),
    start_ts DateTime64(3, 'Europe/Paris'), -- milli seconde
  `duration_s` Float32,
    `co2_ug` Float32,
  `nb_outoftrack` UInt16,
    `nb_gate` UInt8,
    `turn_adjust` UInt8,
    is_turn_valid Boolean
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn);


CREATE TABLE IF NOT EXISTS dr_gold.etl_event_last_session_player_v
(
    `track_num` UInt8,
    `turn` UInt8,
    gate_active_id UInt8,
    event_ts_second DateTime('Europe/Paris'),
    start_ts DateTime64(3, 'Europe/Paris'), -- milli seconde
  `duration_ms` UInt16,
    `co2_ug` Float32,
      is_outoftrack Boolean,
      __inserted_time DateTime('Europe/Paris'),
   `session_id` UInt16,
   `event_id` UInt16,
   `player_id` UInt16,
   `name` String
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn);


CREATE TABLE IF NOT EXISTS dr_gold.etl_event_session_player_v
(
       `session_id` UInt16,
       `event_id` UInt16,
       `player_id` UInt16,
       `name` String,
    `track_num` UInt8,
    `turn` UInt8,
  `duration_s` Float32,
    `co2_ug` Float32,
  `nb_outoftrack` UInt16,
    `nb_gate` UInt8,
    `turn_adjust` UInt8,
    is_turn_valid Boolean
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn);


CREATE TABLE IF NOT EXISTS dr_gold.etl_last_session_turns_v
(
       `session_id` UInt16,
       `event_id` UInt16,
       `player_id` UInt16,
       `name` String,
    `track_num` UInt8,
    `turn` UInt8,
  `duration_s` Float32,
    `co2_ug` Float32,
  `nb_outoftrack` UInt16,
    `nb_gate` UInt8,
    `turn_adjust` UInt8,
    is_turn_valid Boolean
) ENGINE = MergeTree()
ORDER BY
  (event_id, session_id, player_id, track_num, turn);

