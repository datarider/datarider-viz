# Data Rider - VIZ
Improve Mario Kart with IOT and AI

## Description
This repository is dedicated to the DataViz. It display the prepared data from tracks combined with players information.

## Organisation


## Tools
In this project, we use the following tools :
- [Apache Superset](https://superset.apache.org/)
    - Data exploration
    - Visualization platform
    - Open-source
- [ClickHouse]()
  - Database for real-time apps
  - Database for analytics (OLAP)
  - Column-Oriented Databases
  - Open-source

## Setup

### Prerequisites

#### Windows
- WSL
- Docker in WSL

#### Linux/Mac
- Docker


### Easy setup
Run the init script in the root folder of this project :
    
    ./init.sh

In the Superset page, use the import button and load all zip from `docker/superset/dashboard`.

### Manual setup

#### Start the stack
    docker compose up -d

#### Clickhouse
##### Connect to ClickHouse CLI
    docker exec -it clickhouse clickhouse-client

##### Clickhouse UI
Connect automatically to Clickhouse with an UI on http://localhost:5521 

##### Clickhouse UI (alternative)
Use http://dash.tabix.io/ for graphical web interface :
- URL : http://127.0.0.1:8123
- User : (see .env file)
- Password: (see .env file)

#### Superset
##### Connect to Superset
Go to http://localhost:8088 on your web browser.

##### Add Clickhouse connections (manual mode - if automatic creation failed)
    Settings > Database Connections > + Database > Supported Databases > ClickHouse Connect (Superset)
Then click on `Connect with SQL Alchemy instead`
Copy this value `clickhousedb://clickhouse:8123/` in the field SQL ALCHEMY URI.
Then TEST CONNECTION should by OK
Then click on CONNECT !

##### Import Dashboard (mandatory)
Use the import button and load zip from `docker/superset/dashboard`.


## Data Interface Contract

Read on [INTERFACE-CONTRACT.md](INTERFACE-CONTRACT.md)


## Contributing
Orange Business and Business&Decision members only

## Authors and acknowledgment
For this current repository, sorted by alphabetical name:
- DE ROTALIER Maximilien as Data Engineer
- GARRY Freddy as Data Engineer
- LE PENMELEN Cédric as Product Owner & Data Engineer

## License
X11 License Distribution Modification Variant - See LICENSE file